// NvAPI.cpp : Defines the exported functions for the DLL application.
// This DLL project is a mock implementation of the NVidia NvAPI for spying on NvAPI calls
// It should also be able to forward calls to the real NvAPI

#include <Windows.h>
#include "nvapi.h"
#include <fstream>
#include <string>

// Location of real NvAPI DLL files
#ifdef _WIN64
#define NVAPIDLL_1      "C:\\Windows\\System32\\nvapi64_orig.dll"
#define NVAPIDLL_2      "C:\\Windows\\System32\\nvapi64.dll"
#else
#define NVAPIDLL_1      "C:\\Windows\\System32\\nvapi_orig.dll"
#define NVAPIDLL_2      "C:\\Windows\\System32\\nvapi.dll"
#endif

#define NVAPI_ERROR 1

// Pointers to real NvAPI SDK DLL functions
typedef NV_STATUS   (*NVAPI_INITIALIZE)(void);
typedef void*       (*NVAPI_QUERYINTERFACE)(unsigned int function_code);
typedef NV_STATUS   (*NVAPI_I2CWRITEEX)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown);
typedef NV_STATUS   (*NVAPI_I2CREADEX)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown);
typedef NV_STATUS   (*NVAPI_I2CWRITE)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info);
typedef NV_STATUS   (*NVAPI_I2CREAD)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info);
typedef NV_STATUS   (*NVAPI_GPU_QUERYILLUMINATIONSUPPORT)(NV_GPU_QUERY_ILLUMINATION_SUPPORT_PARM* pIlluminationSupportInfo);
typedef NV_STATUS   (*NVAPI_GPU_GETILLUMINATION)(NV_GPU_GET_ILLUMINATION_PARM* pIlluminationInfo);
typedef NV_STATUS   (*NVAPI_GPU_SETILLUMINATION)(NV_GPU_SET_ILLUMINATION_PARM* pIlluminationInfo);
typedef NV_STATUS   (*NVAPI_GPU_CLIENTILLUMDEVICESGETINFO)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_DEVICE_INFO_PARAMS* pIllumDevicesInfo );
typedef NV_STATUS   (*NVAPI_GPU_CLIENTILLUMDEVICESGETCONTROL)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_DEVICE_CONTROL_PARAMS* pClientIllumDevicesControl);
typedef NV_STATUS   (*NVAPI_GPU_CLIENTILLUMDEVICESSETCONTROL)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_DEVICE_CONTROL_PARAMS* pClientIllumDevicesControl);
typedef NV_STATUS   (*NVAPI_GPU_CLIENTILLUMZONESGETINFO)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_ZONE_INFO_PARAMS* pIllumZonesInfo);
typedef NV_STATUS   (*NVAPI_GPU_CLIENTILLUMZONESGETCONTROL)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_ZONE_CONTROL_PARAMS* pIllumZonesControl);
typedef NV_STATUS   (*NVAPI_GPU_CLIENTILLUMZONESSETCONTROL)(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_ZONE_CONTROL_PARAMS* pIllumZonesControl);

// Module pointer for real NvAPI
HMODULE hModule = NULL;
bool opened_file = false;

std::ofstream outfile;

enum I2C_TYPE
{
    I2C_WRITE_EX,
    I2C_READ_EX,
    I2C_WRITE,
    I2C_READ

};

NVAPI_INITIALIZE                        _NvAPI_Initialize;
NVAPI_QUERYINTERFACE                    _NvAPI_QueryInterface;
NVAPI_I2CWRITEEX                        _NvAPI_I2CWriteEx;
NVAPI_I2CREADEX                         _NvAPI_I2CReadEx;
NVAPI_I2CWRITE                          _NvAPI_I2CWrite;
NVAPI_I2CREAD                           _NvAPI_I2CRead;
NVAPI_GPU_QUERYILLUMINATIONSUPPORT      _NvAPI_GPU_QueryIlluminationSupport;
NVAPI_GPU_GETILLUMINATION               _NvAPI_GPU_GetIllumination;
NVAPI_GPU_SETILLUMINATION               _NvAPI_GPU_SetIllumination;
NVAPI_GPU_CLIENTILLUMDEVICESGETINFO     _NvAPI_GPU_ClientIllumDevicesGetInfo;
NVAPI_GPU_CLIENTILLUMDEVICESGETCONTROL  _NvAPI_GPU_ClientIllumDevicesGetControl;
NVAPI_GPU_CLIENTILLUMDEVICESSETCONTROL  _NvAPI_GPU_ClientIllumDevicesSetControl;
NVAPI_GPU_CLIENTILLUMZONESGETINFO       _NvAPI_GPU_ClientIllumZonesGetInfo;
NVAPI_GPU_CLIENTILLUMZONESGETCONTROL    _NvAPI_GPU_ClientIllumZonesGetControl;
NVAPI_GPU_CLIENTILLUMZONESSETCONTROL    _NvAPI_GPU_ClientIllumZonesSetControl;

// Concatenate together a massive string to dump to the file, filtering first by control mode and then zone type
// to determine what all information needs to be printed as to not overwhelm the user
std::string createZoneCtrlString(NV_GPU_CLIENT_ILLUM_ZONE_CONTROL_PARAMS* pIllumZonesControl, std::string controlType) 
{
    unsigned int size = 40;
    std::string blanks(size, ' ');
    std::string dash(size, '-');
    std::string output = "";
    output += controlType + "   ";
    output += "version: " + std::to_string(pIllumZonesControl->version);
    output += "  numIllumZones: " + std::to_string(pIllumZonesControl->numIllumZonesControl);
    output += "  bDefault: " + std::to_string(pIllumZonesControl->bDefault);
    output += "  rsvdField: " + std::to_string(pIllumZonesControl->rsvdField) + "\n";
    // Not gonna bother printing this because I doubt it has much value: rsvd[64]
    for (unsigned int i = 0; i < pIllumZonesControl->numIllumZonesControl; i++)
    {
        output += blanks + "ZoneIdx: " + std::to_string(i) + " " + dash + "\n";
        output += blanks + "**ZoneType: ";
        switch (pIllumZonesControl->zones[i].type)
        {
            case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_INVALID:
                output += "INVALID";
                break;
            case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_RGB:
                output += "RGB";
                break;
            case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_COLOR_FIXED:
                output += "COLOR_FIXED";
                break;
            case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_RGBW:
                output += "RGBW";
                break;
            case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_SINGLE_COLOR:
                output += "SINGLE_COLOR";
                break;
            default:
                output += std::to_string(pIllumZonesControl->zones[i].type) + " (unknown)";
        }
        output += "  ControlMode: ";
        switch (pIllumZonesControl->zones[i].ctrlMode)
        {
            case NV_GPU_CLIENT_ILLUM_CTRL_MODE_MANUAL:
                output += "MANUAL\n";
                switch(pIllumZonesControl->zones[i].type)
                {
                    case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_INVALID:
                        output += blanks + "INVALID, uncertain which info to print";
                        break;
                    case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_RGB:
                        output += blanks + "**DATA_RGB::";
                        output += "  Red: " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.manualRGB.rgbParams.colorR);
                        output += "  Green: " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.manualRGB.rgbParams.colorG);
                        output += "  Blue: " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.manualRGB.rgbParams.colorB);
                        output += "  Brightness%: " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.manualRGB.rgbParams.brightnessPct);
                        break;
                    case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_COLOR_FIXED:
                        output += blanks + "**DATA_COLOR_FIXED::";
                        output += "  ManualFixedBrightness%: " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.manualColorFixed.colorFixedParams.brightnessPct);
                        break;
                    case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_RGBW:
                        output += blanks + "**DATA_RGBW::";
                        output += "  Red: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.manualRGBW.rgbwParams.colorR);
                        output += "  Green: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.manualRGBW.rgbwParams.colorG);
                        output += "  Blue: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.manualRGBW.rgbwParams.colorB);
                        output += "  White: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.manualRGBW.rgbwParams.colorW);
                        output += "  Brightness%: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.manualRGBW.rgbwParams.brightnessPct);
                        break;
                    case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_SINGLE_COLOR:
                        output += blanks + "**DATA_SINGLE_COLOR::";
                        output += "  Brightness% " + std::to_string(pIllumZonesControl->zones[i].data.singleColor.data.manualSingleColor.singleColorParams.brightnessPct);
                        break;
                    default:
                        output += blanks + std::to_string(pIllumZonesControl->zones[i].type) + " (zone type unknown, uncertain of which data to print)";
                }
                break;
            case NV_GPU_CLIENT_ILLUM_CTRL_MODE_PIECEWISE_LINEAR:
                output += "PIECEWISE_LINEAR\n";
                switch (pIllumZonesControl->zones[i].type)
                {
                    case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_INVALID:
                        output += blanks + "INVALID, uncertain which info to print";
                        break;
                    case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_RGB:
                        output += blanks + "**DATA_RGB::\n";
                        output += blanks + "****CycleType: ";
                        switch (pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.piecewiseLinearData.cycleType)
                        {
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_HALF_HALT:
                                output += "CYCLE_HALF_HALT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_FULL_HALT:
                                output += "CYCLE_FULL_HALT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_FULL_REPEAT:
                                output += "CYCLE_FULL_REPEAT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_INVALID:
                                output += "INVALID";
                                break;
                            default:
                                output += std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.piecewiseLinearData.cycleType) + " (unknown)";
                        }
                        output += "\n";
                        output += blanks + "****CycleRepeat Count: " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.piecewiseLinearData.grpCount);
                        output += "  colorA->BswitchTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.piecewiseLinearData.riseTimems);
                        output += "  colorB->AswitchTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.piecewiseLinearData.fallTimems);
                        output += "  colorAtime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.piecewiseLinearData.ATimems);
                        output += "  colorBtime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.piecewiseLinearData.BTimems);
                        output += "  IdleTimeBeforeNextGroup(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.piecewiseLinearData.grpIdleTimems);
                        output += "  zoneOffsetTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.piecewiseLinearData.phaseOffsetms);
                        output += "\n";
                        output += blanks + "**DATA_PIECEWISE_LINEAR_ENDPOINTS\n";
                        for (unsigned int j = 0; j < NV_GPU_CLIENT_ILLUM_CTRL_MODE_PIECEWISE_LINEAR_COLOR_ENDPOINTS; j++)
                        {
                            output += blanks + "****ENDPOINT_NUM" + std::to_string(j);
                            output += "  Red: " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.rgbParams[j].colorR);
                            output += "  Green: " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.rgbParams[j].colorG);
                            output += "  Blue: " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.rgbParams[j].colorB);
                            output += "  Brightness%: " + std::to_string(pIllumZonesControl->zones[i].data.rgb.data.piecewiseLinearRGB.rgbParams[j].brightnessPct);
                            output += "\n";
                        }
                        break;
                    case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_COLOR_FIXED:
                        output += blanks + "**DATA_COLOR_FIXED::";
                        output += "  LinearPiecewiseBrightness1%: " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.colorFixedParams[0].brightnessPct);
                        output += "  LinearPiecewiseBrightness2%: " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.colorFixedParams[1].brightnessPct);
                        output += "\n";
                        output += blanks;
                        output += "****CycleType: ";
                        switch (pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.piecewiseLinearData.cycleType)
                        {
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_HALF_HALT:
                                output += "CYCLE_HALF_HALT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_FULL_HALT:
                                output += "CYCLE_FULL_HALT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_FULL_REPEAT:
                                output += "CYCLE_FULL_REPEAT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_INVALID:
                                output += "INVALID";
                                break;
                            default:
                                output += std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.piecewiseLinearData.cycleType) + " (unknown)";
                        }
                        output += "\n";
                        output += blanks + "****CycleRepeatCount: " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.piecewiseLinearData.grpCount);
                        output += "  colorA->BswitchTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.piecewiseLinearData.riseTimems);
                        output += "  colorB->AswitchTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.piecewiseLinearData.fallTimems);
                        output += "  colorAtime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.piecewiseLinearData.ATimems);
                        output += "  colorBtime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.piecewiseLinearData.BTimems);
                        output += "  IdleTimeBeforeNextGroup(ms): " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.piecewiseLinearData.grpIdleTimems);
                        output += "  zoneOffsetTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.piecewiseLinearData.phaseOffsetms);
                        for (unsigned int j = 0; j < NV_GPU_CLIENT_ILLUM_CTRL_MODE_PIECEWISE_LINEAR_COLOR_ENDPOINTS; j++)
                        {
                            output += blanks + "****ENDPOINT_NUM" + std::to_string(j);
                            output += "  Brightness%: " + std::to_string(pIllumZonesControl->zones[i].data.colorFixed.data.piecewiseLinearColorFixed.colorFixedParams[j].brightnessPct);
                            output += "\n";
                        }
                        break;
                    case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_RGBW:
                        output += blanks + "**DATA_RGBW";
                        output += blanks + "****CycleType: ";
                        switch (pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.piecewiseLinearData.cycleType)
                        {
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_HALF_HALT:
                                output += "CYCLE_HALF_HALT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_FULL_HALT:
                                output += "CYCLE_FULL_HALT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_FULL_REPEAT:
                                output += "CYCLE_FULL_REPEAT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_INVALID:
                                output += "INVALID";
                                break;
                            default:
                                output += std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.piecewiseLinearData.cycleType) + " (unknown)";
                        }
                        output += "\n";
                        output += blanks + "****CycleRepeat Count: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.piecewiseLinearData.grpCount);
                        output += "  colorA->BswitchTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.piecewiseLinearData.riseTimems);
                        output += "  colorB->AswitchTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.piecewiseLinearData.fallTimems);
                        output += "  colorAtime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.piecewiseLinearData.ATimems);
                        output += "  colorBtime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.piecewiseLinearData.BTimems);
                        output += "  IdleTimeBeforeNextGroup(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.piecewiseLinearData.grpIdleTimems);
                        output += "  zoneOffsetTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.piecewiseLinearData.phaseOffsetms);
                        output += "\n";
                        output += blanks + "**DATA_PIECEWISE_LINEAR_ENDPOINTS\n";
                        for (unsigned int j = 0; j < NV_GPU_CLIENT_ILLUM_CTRL_MODE_PIECEWISE_LINEAR_COLOR_ENDPOINTS; j++)
                        {
                            output += blanks + "****ENDPOINT_NUM" + std::to_string(j);
                            output += "  Red: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.rgbwParams[j].colorR);
                            output += "  Green: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.rgbwParams[j].colorG);
                            output += "  Blue: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.rgbwParams[j].colorB);
                            output += "  White: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.rgbwParams[j].colorW);
                            output += "  Brightness%: " + std::to_string(pIllumZonesControl->zones[i].data.rgbw.data.piecewiseLinearRGBW.rgbwParams[j].brightnessPct);
                            output += "\n";
                        }
                        break;
                    case NV_GPU_CLIENT_ILLUM_ZONE_TYPE_SINGLE_COLOR:
                        output += blanks + "**DATA_SINGLE_COLOR";
                        output += blanks + "****CycleType: ";
                        switch (pIllumZonesControl->zones[i].data.singleColor.data.piecewiseLinearSingleColor.piecewiseLinearData.cycleType)
                        {
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_HALF_HALT:
                                output += "CYCLE_HALF_HALT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_FULL_HALT:
                                output += "CYCLE_FULL_HALT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_FULL_REPEAT:
                                output += "CYCLE_FULL_REPEAT";
                                break;
                            case NV_GPU_CLIENT_ILLUM_PIECEWISE_LINEAR_CYCLE_INVALID:
                                output += "INVALID";
                                break;
                            default:
                                output += std::to_string(pIllumZonesControl->zones[i].data.singleColor.data.piecewiseLinearSingleColor.piecewiseLinearData.cycleType) + " (unknown)";
                        }
                        output += "\n";
                        output += blanks + "****CycleRepeat Count: " + std::to_string(pIllumZonesControl->zones[i].data.singleColor.data.piecewiseLinearSingleColor.piecewiseLinearData.grpCount);
                        output += "  colorA->BswitchTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.singleColor.data.piecewiseLinearSingleColor.piecewiseLinearData.riseTimems);
                        output += "  colorB->AswitchTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.singleColor.data.piecewiseLinearSingleColor.piecewiseLinearData.fallTimems);
                        output += "  colorAtime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.singleColor.data.piecewiseLinearSingleColor.piecewiseLinearData.ATimems);
                        output += "  colorBtime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.singleColor.data.piecewiseLinearSingleColor.piecewiseLinearData.BTimems);
                        output += "  IdleTimeBeforeNextGroup(ms): " + std::to_string(pIllumZonesControl->zones[i].data.singleColor.data.piecewiseLinearSingleColor.piecewiseLinearData.grpIdleTimems);
                        output += "  zoneOffsetTime(ms): " + std::to_string(pIllumZonesControl->zones[i].data.singleColor.data.piecewiseLinearSingleColor.piecewiseLinearData.phaseOffsetms);
                        output += "\n";
                        output += blanks + "**DATA_PIECEWISE_LINEAR_ENDPOINTS\n";
                        for (unsigned int j = 0; j < NV_GPU_CLIENT_ILLUM_CTRL_MODE_PIECEWISE_LINEAR_COLOR_ENDPOINTS; j++)
                        {
                            output += blanks + "****ENDPOINT_NUM" + std::to_string(j);
                            output += "  Brightness%: " + std::to_string(pIllumZonesControl->zones[i].data.singleColor.data.piecewiseLinearSingleColor.singleColorParams[j].brightnessPct);
                            output += "\n";
                        }
                        break;
                    default:
                        output += std::to_string(pIllumZonesControl->zones[i].type) + " (unknown, uncertain of which data to print)";
                }
                break;
            case NV_GPU_CLIENT_ILLUM_CTRL_MODE_INVALID:
                output += "INVALID\n";
                break;
            default:
                output += std::to_string(pIllumZonesControl->zones[i].ctrlMode) + " (unknown, uncertain of which data to print)\n";
        }
		output += "\n";
    }
	return  output;
}

void stream_I2C_to_file(I2C_TYPE api_call_type, NV_I2C_INFO_V3* i2c_info)
{
    char hex_buf[7];

    snprintf(hex_buf, 5, "0x%02X", i2c_info->i2c_dev_address >> 1);
    switch(api_call_type)
    {
        case I2C_READ:
            outfile << "NvAPI_I2CRead:  Dev: ";
            break;
        case I2C_WRITE:
            outfile << "NvAPI_I2CWrite:  Dev: ";
            break;
        case I2C_READ_EX:
            outfile << "NvAPI_I2CReadEx:  Dev: ";
            break;
        case I2C_WRITE_EX:
            outfile << "NvAPI_I2CWriteEx:  Dev: ";
            break;
    }
    outfile << hex_buf;

    snprintf(hex_buf, 5, "0x%02X", i2c_info->reg_addr_size);
    outfile << " RegSize: ";
    outfile << hex_buf;

    outfile << " Reg:";

    for (unsigned int i = 0; i < i2c_info->reg_addr_size; i++)
    {
        outfile << " ";
        snprintf(hex_buf, 5, "0x%02X", i2c_info->i2c_reg_address[i]);
        outfile << hex_buf;
    }

    snprintf(hex_buf, 5, "0x%02X", i2c_info->size);
    outfile << " Size: ";
    outfile << hex_buf;

    outfile << " Data:";

    for (unsigned int i = 0; i < i2c_info->size; i++)
    {
        outfile << " ";
        snprintf(hex_buf, 5, "0x%02X", i2c_info->data[i]);
        outfile << hex_buf;
    }

    outfile << std::endl;

}

extern "C"
{
    __declspec(dllexport) void* nvapi_QueryInterface(unsigned int function_code)
    {
        void* ret_val = NULL;

        printf("nvapi_QueryInterface\r\n");
        if (!hModule)
        {
            hModule = LoadLibrary(NVAPIDLL_1);

            if (!hModule)
            {
                hModule = LoadLibrary(NVAPIDLL_2);
            }

            if (hModule)
            {
                _NvAPI_QueryInterface                   = (NVAPI_QUERYINTERFACE)GetProcAddress(hModule, "nvapi_QueryInterface");

                _NvAPI_I2CWriteEx                       = (NVAPI_I2CWRITEEX)_NvAPI_QueryInterface(0x283AC65A);
                _NvAPI_I2CReadEx                        = (NVAPI_I2CREADEX)_NvAPI_QueryInterface(0x4D7B0709);

                _NvAPI_I2CWrite                         = (NVAPI_I2CWRITE)_NvAPI_QueryInterface(0x0E812EB07);
                _NvAPI_I2CRead                          = (NVAPI_I2CREAD)_NvAPI_QueryInterface(0x2FDE12C5);

                _NvAPI_GPU_QueryIlluminationSupport     = (NVAPI_GPU_QUERYILLUMINATIONSUPPORT)_NvAPI_QueryInterface(0xA629DA31);
                _NvAPI_GPU_GetIllumination              = (NVAPI_GPU_GETILLUMINATION)_NvAPI_QueryInterface(0x9A1B9365);
                _NvAPI_GPU_SetIllumination              = (NVAPI_GPU_SETILLUMINATION)_NvAPI_QueryInterface(0x0254A187);

                _NvAPI_GPU_ClientIllumDevicesGetInfo    = (NVAPI_GPU_CLIENTILLUMDEVICESGETINFO)_NvAPI_QueryInterface(0xD4100E58);
                _NvAPI_GPU_ClientIllumDevicesGetControl = (NVAPI_GPU_CLIENTILLUMDEVICESGETCONTROL)_NvAPI_QueryInterface(0x73C01D58);
                _NvAPI_GPU_ClientIllumDevicesSetControl = (NVAPI_GPU_CLIENTILLUMDEVICESSETCONTROL)_NvAPI_QueryInterface(0x57024C62);

                _NvAPI_GPU_ClientIllumZonesGetInfo      = (NVAPI_GPU_CLIENTILLUMZONESGETINFO)_NvAPI_QueryInterface(0x4B81241B);
                _NvAPI_GPU_ClientIllumZonesGetControl   = (NVAPI_GPU_CLIENTILLUMZONESGETCONTROL)_NvAPI_QueryInterface(0x3DBF5764);
                _NvAPI_GPU_ClientIllumZonesSetControl   = (NVAPI_GPU_CLIENTILLUMZONESSETCONTROL)_NvAPI_QueryInterface(0x197D065E);
            }
        }

        if (hModule)
        {
            // Get actual value
            ret_val = _NvAPI_QueryInterface(function_code);

            // Override if it's a function we fake
            switch (function_code)
            {
            case 0x283AC65A:
                ret_val = &NvAPI_I2CWriteEx;
                break;
            case 0x4D7B0709:
                ret_val = &NvAPI_I2CReadEx;
                break;
            case 0x0E812EB07:
                ret_val = &NvAPI_I2CWrite;
                break;
            case 0x2FDE12C5:
                ret_val = &NvAPI_I2CRead;
                break;
            case 0xA629DA31:
                ret_val = &NvAPI_GPU_QueryIlluminationSupport;
                break;
            case 0x9A1B9365:
                ret_val = &NvAPI_GPU_GetIllumination;
                break;
            case 0x0254A187:
                ret_val = &NvAPI_GPU_SetIllumination;
                break;
            case 0xD4100E58:
                ret_val = &NvAPI_GPU_ClientIllumDevicesGetInfo;
                break;
            case 0x73C01D58:
                ret_val = &NvAPI_GPU_ClientIllumDevicesGetControl;
                break;
            case 0x57024C62:
                ret_val = &NvAPI_GPU_ClientIllumDevicesSetControl;
                break; 
            case 0x4B81241B:
                ret_val = &NvAPI_GPU_ClientIllumZonesGetInfo;
                break;
            case 0x3DBF5764:
                ret_val = &NvAPI_GPU_ClientIllumZonesGetControl;
                break;
            case 0x197D065E:
                ret_val = &NvAPI_GPU_ClientIllumZonesSetControl;
                break;
            }
        }

        return(ret_val);
    }
}

NV_STATUS NvAPI_I2CWriteEx(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if(!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if(hModule)
    {
        stream_I2C_to_file(I2C_WRITE_EX, i2c_info);
        ret_val = _NvAPI_I2CWriteEx(physical_gpu_handle, i2c_info, unknown);
    }

    return(ret_val);
}

NV_STATUS NvAPI_I2CReadEx(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info, NV_U32* unknown)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {
        ret_val = _NvAPI_I2CReadEx(physical_gpu_handle, i2c_info, unknown);
        stream_I2C_to_file(I2C_READ_EX, i2c_info);
    }

    return(ret_val);
}

NV_STATUS NvAPI_I2CWrite(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if(!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if(hModule)
    {
        stream_I2C_to_file(I2C_WRITE, i2c_info);
        ret_val = _NvAPI_I2CWrite(physical_gpu_handle, i2c_info);
    }

    return(ret_val);
}

NV_STATUS NvAPI_I2CRead(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_I2C_INFO_V3* i2c_info)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {
        ret_val = _NvAPI_I2CRead(physical_gpu_handle, i2c_info);
        stream_I2C_to_file(I2C_READ, i2c_info);
    }

    return(ret_val);
}

NV_STATUS NvAPI_GPU_QueryIlluminationSupport(NV_GPU_QUERY_ILLUMINATION_SUPPORT_PARM* pIlluminationSupportInfo)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {

        outfile << "NvAPI_GPU_QueryIlluminationSupport: ";
       
        ret_val = _NvAPI_GPU_QueryIlluminationSupport(pIlluminationSupportInfo);

        if(pIlluminationSupportInfo->Attribute == NV_GPU_IA_LOGO_BRIGHTNESS)
        {
           outfile << "Logo Support: "  << std::boolalpha << pIlluminationSupportInfo->bSupported;
        }
        else if (pIlluminationSupportInfo->Attribute == NV_GPU_IA_SLI_BRIGHTNESS)
        {
            outfile << "SLI Support: " << std::boolalpha << pIlluminationSupportInfo->bSupported;
        }
        else
        {
            outfile << "Enum unrecognized!";
        }
        outfile << std::endl;
    }

    return(ret_val);
}

NV_STATUS NvAPI_GPU_GetIllumination(NV_GPU_GET_ILLUMINATION_PARM* pIlluminationInfo)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {

        outfile << "NvAPI_GPU_GetIlluminationParm: ";

        ret_val = _NvAPI_GPU_GetIllumination(pIlluminationInfo);

        if (pIlluminationInfo->Attribute == NV_GPU_IA_LOGO_BRIGHTNESS)
        {
            outfile << "Logo Value: "  << pIlluminationInfo->Value;
        }
        else if (pIlluminationInfo->Attribute == NV_GPU_IA_SLI_BRIGHTNESS)
        {
            outfile << "SLI Value: "  << pIlluminationInfo->Value;
        }
        else
        {
            outfile << "Enum unrecognized!";
        }
        outfile << std::endl;
    }

    return(ret_val);
}

NV_STATUS NvAPI_GPU_SetIllumination(NV_GPU_SET_ILLUMINATION_PARM* pIlluminationInfo)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {
        outfile << "NvAPI_GPU_SettIlluminationParm: ";
        if (pIlluminationInfo->Attribute == NV_GPU_IA_LOGO_BRIGHTNESS)
        {
            outfile << "Logo Value: " << pIlluminationInfo->Value;
        }
        else if (pIlluminationInfo->Attribute == NV_GPU_IA_SLI_BRIGHTNESS)
        {
            outfile << "SLI Value: " << pIlluminationInfo->Value;
        }
        else
        {
            outfile << "Enum unrecognized!";
        }
        outfile << std::endl;
        ret_val = _NvAPI_GPU_SetIllumination(pIlluminationInfo);
    }

    return(ret_val);
}

NV_STATUS NvAPI_GPU_ClientIllumDevicesGetInfo(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_DEVICE_INFO_PARAMS* pIllumDevicesInfo)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {
        ret_val = _NvAPI_GPU_ClientIllumDevicesGetInfo(physical_gpu_handle, pIllumDevicesInfo);
        outfile << "NvAPI_GPU_ClientIllumDevicesGetInfo: ";
        outfile << "version: " << pIllumDevicesInfo->version;
        outfile << "numIllumDevices: " << pIllumDevicesInfo->numIllumDevices;
        outfile << "rsvd: " << pIllumDevicesInfo->rsvd;
        outfile << std::endl;
        
    }

    return(ret_val);
}

NV_STATUS NvAPI_GPU_ClientIllumDevicesGetControl(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_DEVICE_CONTROL_PARAMS* pClientIllumDevicesControl)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {
        ret_val = _NvAPI_GPU_ClientIllumDevicesGetControl(physical_gpu_handle, pClientIllumDevicesControl);
        outfile << "NvAPI_GPU_ClientIllumDevicesGetControl: ";
        outfile << "version: " << pClientIllumDevicesControl->version;
        outfile << "numIllumDevices: " << pClientIllumDevicesControl->numIllumDevices;
        outfile << "rsvd: " << pClientIllumDevicesControl->rsvd;
        outfile << std::endl;
    }

    return(ret_val);
}

NV_STATUS NvAPI_GPU_ClientIllumDevicesSetControl(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_DEVICE_CONTROL_PARAMS* pClientIllumDevicesControl)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {
        outfile << "NvAPI_GPU_ClientIllumDevicesSetControl: ";
        outfile << "version: " << pClientIllumDevicesControl->version;
        outfile << "numIllumDevices: " << pClientIllumDevicesControl->numIllumDevices;
        outfile << "rsvd: " << pClientIllumDevicesControl->rsvd;
        outfile << std::endl;
        ret_val = _NvAPI_GPU_ClientIllumDevicesSetControl(physical_gpu_handle, pClientIllumDevicesControl);
    }

    return(ret_val);
}

NV_STATUS NvAPI_GPU_ClientIllumZonesGetInfo(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_ZONE_INFO_PARAMS* pIllumZonesInfo)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {
        unsigned int size = 38;
        std::string blanks(size, ' ');
        char hex_buf[7];
        ret_val = _NvAPI_GPU_ClientIllumZonesGetInfo(physical_gpu_handle, pIllumZonesInfo);
        outfile << "NvAPI_GPU_ClientIllumZonesGetInfo:   ";
        outfile << "  version: " << pIllumZonesInfo->version;
        outfile << "  numIllumZones: " << pIllumZonesInfo->numIllumZones;
        for (unsigned int i = 0; i < pIllumZonesInfo->numIllumZones; i++)
        {
            outfile << std::endl;
            outfile << blanks << "  ZoneProvIdx: " << pIllumZonesInfo->zones[i].provIdx;
            outfile << "  RGB:  ";
            snprintf(hex_buf, 5, "0x%02X", pIllumZonesInfo->zones[i].data.rgb.rsvd);
            outfile << hex_buf;
        }
        outfile << std::endl;
    }

    return(ret_val);
}

NV_STATUS NvAPI_GPU_ClientIllumZonesGetControl(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_ZONE_CONTROL_PARAMS* pIllumZonesControl)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {
        ret_val = _NvAPI_GPU_ClientIllumZonesGetControl(physical_gpu_handle, pIllumZonesControl);
        std::string writeOut = createZoneCtrlString(pIllumZonesControl, "NvAPI_GPU_ClientIllumZonesGetControl:");
        outfile << writeOut;
    }
    

    return(ret_val);
}

NV_STATUS NvAPI_GPU_ClientIllumZonesSetControl(NV_PHYSICAL_GPU_HANDLE physical_gpu_handle, NV_GPU_CLIENT_ILLUM_ZONE_CONTROL_PARAMS* pIllumZonesControl)
{
    NV_STATUS ret_val = NVAPI_ERROR;

    if (!opened_file)
    {
        outfile.open("C:\\NvAPISpy\\nvapi_" + std::to_string(GetTickCount()) + ".txt", std::ofstream::out);
        opened_file = true;
    }

    if (hModule)
    {
        std::string writeOut = createZoneCtrlString(pIllumZonesControl, "NvAPI_GPU_ClientIllumZonesSetControl:");
        outfile << writeOut;
        ret_val = _NvAPI_GPU_ClientIllumZonesSetControl(physical_gpu_handle, pIllumZonesControl);
    }

    return(ret_val);
}